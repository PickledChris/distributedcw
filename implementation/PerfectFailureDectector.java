package implementation;

import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import provided.IFailureDetector;
import provided.Message;
import provided.Process;
import provided.Utils;

public class PerfectFailureDectector implements IFailureDetector {

	protected static final int DELTA = 1000; /* 1sec */
	protected static final long SUSPECT_TIME = DELTA + Utils.DELAY;
	protected Process p;
	protected Timer heartbeat;
	protected ScheduledExecutorService timeout;
	protected Set<Integer> suspects;
	protected Map<Integer, Long> processLastContact = new ConcurrentHashMap<Integer, Long>();

	public PerfectFailureDectector(Process p) {
		this.p = p;
		this.heartbeat = new Timer();
		this.timeout = new ScheduledThreadPoolExecutor(3);
		this.suspects = new HashSet<Integer>();
	}

	private class PeriodicTask extends TimerTask {
		public void run() {
			p.broadcast("heartbeat", null);
		}
	}
	
	private class Timeout implements Runnable {
		public void run() {
			long start = System.currentTimeMillis();

			try {
				Thread.sleep(SUSPECT_TIME);
			} catch (InterruptedException e) { e.printStackTrace();}
			
			for (Entry<Integer, Long> procTime : processLastContact.entrySet()) {
				if (procTime.getValue() < start){
					isSuspected(procTime.getKey());
					Utils.out(String.format("Process %d suspects %d", p.pid, procTime.getKey()));
				} else {
					suspects.remove(procTime.getKey());
				}
			}
		}
	}

	@Override
	public void begin() {
		// Set all other processes to have last clocked in now
		resetLastContact();
		heartbeat.schedule(new PeriodicTask(), 0, DELTA);
		timeout.scheduleAtFixedRate(new Timeout(), 0, DELTA, TimeUnit.MILLISECONDS);
	}

	@Override
	public void receive(Message m) {
		processLastContact.put(m.getSource(), System.currentTimeMillis());
	}

	@Override
	public boolean isSuspect(Integer process) {
		return suspects.contains(process);
	}

	@Override
	public int getLeader() {
		return -1;
	}

	@Override
	public void isSuspected(Integer process) {
		suspects.add(process);
	}

	@Override
	public int consensus(int value) {
		throw new UnsupportedOperationException();
	}

	private void resetLastContact() {
		for (int i = 1; i <= p.n; i++) {
			if (i != p.pid)
				processLastContact.put(i, System.currentTimeMillis());
		}
	}
}
