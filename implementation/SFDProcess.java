package implementation;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import provided.IFailureDetector;
import provided.Message;

public class SFDProcess extends ProcessImplementation {

	private IFailureDetector detector;
	private int value;
	
	public SFDProcess(String name, int id, int size, int value) {
		super(name, id, size);
		detector = new StrongFailureDectector(this);
		this.value = value;
	}

	public void begin() {
		detector.begin();
	}

	public synchronized void receive(Message m) {
		String type = m.getType();
		if (type.equals("heartbeat") || type.equals("VAL")) {
			detector.receive(m);
		}
	}
	
	@Override
	public int consensus() {
		return detector.consensus(value);
	}

	public static void main(String[] args) {

		SFDProcess p1 = new SFDProcess("P1", 1, 3, 42);
		SFDProcess p2 = new SFDProcess("P2", 2, 3, 42);
		SFDProcess p3 = new SFDProcess("P3", 3, 3, 42);

		ExecutorService executor = Executors.newFixedThreadPool(3);
		executor.submit(new ProcessRegisterer(p1));
		executor.submit(new ProcessRegisterer(p2));
		executor.submit(new ProcessRegisterer(p3));
		
		/* For delaying to fault inject
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
		} */
		Future<Integer> p1Con = executor.submit(new ConsensusTask(p1));
		Future<Integer> p2Con = executor.submit(new ConsensusTask(p2));
		Future<Integer> p3Con = executor.submit(new ConsensusTask(p3));

		try {
			System.out.println("1: " + p1Con.get());
			System.out.println("2: " + p2Con.get());
			System.out.println("3: " + p3Con.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
}