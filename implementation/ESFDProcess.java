package implementation;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import provided.IFailureDetector;
import provided.Message;

public class ESFDProcess extends ProcessImplementation {

	private IFailureDetector detector;
	private int value;

	public ESFDProcess(String name, int id, int size, int value) {
		super(name, id, size);
		detector = new EventuallyStrongFailureDectector(this);
		this.value = value;
	}

	public void begin() {
		detector.begin();
	}

	public synchronized void receive(Message m) {
		String type = m.getType();
		if (type.equals("heartbeat") || type.equals("VAL") || type.equals("OUTCOME"))  {
			detector.receive(m);
		}
	}

	@Override
	public int consensus() {
		return detector.consensus(value);
	}

	public static void main(String[] args) {
		ESFDProcess p1 = new ESFDProcess("P1", 1, 4, 42);
		ESFDProcess p2 = new ESFDProcess("P2", 2, 4, 42);
		ESFDProcess p3 = new ESFDProcess("P3", 3, 4, 42);
		ESFDProcess p4 = new ESFDProcess("P4", 4, 4, 42);

		ExecutorService executor = Executors.newFixedThreadPool(4);
		executor.submit(new ProcessRegisterer(p1));
		executor.submit(new ProcessRegisterer(p2));
		executor.submit(new ProcessRegisterer(p3));
		executor.submit(new ProcessRegisterer(p4));
		
		//For delaying to fault inject 
		/*
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {} */
		Future<Integer> p1Con = executor.submit(new ConsensusTask(p1));
		Future<Integer> p2Con = executor.submit(new ConsensusTask(p2));
		Future<Integer> p3Con = executor.submit(new ConsensusTask(p3));
		Future<Integer> p4Con = executor.submit(new ConsensusTask(p4));

		safePrint("1: ", p1Con);
		safePrint("2: ", p2Con);
		safePrint("3: ", p3Con);
		safePrint("4: ", p4Con);
	}

	private static void safePrint(String string, Future<Integer> future) {
		try {
			System.out.println(string + future.get(15, TimeUnit.SECONDS));
		} catch (Exception e) {
			System.out.println(string + "failed somewhere");
		}
	}
}