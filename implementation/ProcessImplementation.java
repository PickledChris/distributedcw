package implementation;

import provided.Process;

public abstract class ProcessImplementation extends Process{
	public ProcessImplementation(String name, int pid, int n) {
		super(name, pid, n);
	}

	public abstract void begin();
	
	public abstract int consensus();

}
