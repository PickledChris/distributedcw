package implementation;

public class ProcessRegisterer implements Runnable {

	private ProcessImplementation p;
	public ProcessRegisterer(ProcessImplementation p) {
		this.p = p;
	}
	
	@Override
	public void run() {
		p.registeR();
		p.begin();
	}
	
}
