package implementation;

import static implementation.StrongFailureDectector.maxEntry;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

import provided.IFailureDetector;
import provided.Message;
import provided.Process;
import provided.Utils;

public class EventuallyStrongFailureDectector extends EventuallyPerfectFailureDectector implements IFailureDetector {

	private int minimumMessages = p.n - (p.n / 3) - 1;
	
	private CountDownLatch uponValLatch = new CountDownLatch(minimumMessages);
	private Map<Integer, Integer> valMessages = new ConcurrentHashMap<Integer, Integer>();

	private Map<Integer, String> outcomeMessages = new ConcurrentHashMap<Integer, String>();
	private Map<Integer, CountDownLatch> outcomeLatches = new ConcurrentHashMap<Integer, CountDownLatch>();

	public EventuallyStrongFailureDectector(Process p) {
		super(p);
	}

	@Override
	public void begin() {
		super.begin();
		for (int i = 1; i <= p.n; i++) {
			outcomeLatches.put(i, new CountDownLatch(1));
		}
	}

	@Override
	public void receive(Message m) {
		if (m.getType().equals("heartbeat")){
			super.receive(m);
		} else if (m.getType().equals("VAL")) {
			valMessages.put(m.getSource(), Integer.parseInt(m.getPayload()));
			uponValLatch.countDown();
		} else if (m.getType().equals("OUTCOME")) {
			outcomeMessages.put(m.getSource(), m.getPayload());
			outcomeLatches.get(m.getSource()).countDown();
		}
	}

	@Override
	public void isSuspected(Integer process) {
		super.isSuspected(process);
		outcomeLatches.get(process).countDown();
	}

	@Override
	public int consensus(int value) {
		int candidateVal = value; 
		int round = 0; 
		int m = 0;
		
		while (true) {
			round = round + 1;
			int candidate = (round % p.n) + 1;
			Message messageToSend = new Message(p.pid, candidate, "VAL", String.format("%d", candidateVal));
			p.unicast(messageToSend);
			
			if (p.pid == candidate){
				try {
					uponValLatch.await();
				} catch (InterruptedException e) {}
				int majority = majority(valMessages, candidateVal);
				boolean unique = (new HashSet<Integer>(valMessages.values()).size() ==1);
				p.broadcast("OUTCOME", String.format("%d:%b", majority , unique));
			} else {
				if (collectOutcome(candidate)) {
					String[] payload = outcomeMessages.get(candidate).split(":");
					int majority = Integer.parseInt(payload[0]);
					boolean unique = Boolean.parseBoolean(payload[1]);
					
					if (unique){
						Utils.out(String.format("Process %d in iteration %d, m= %d, c=%d", p.pid, round, candidate, m));
						candidateVal = majority;
						if (m == candidate){
							break;
						} else if (m == 0) {
							m = candidate;
						}
					}
				}
			}
		}
		return candidateVal;
	}
	
	private int majority(Map<Integer, Integer> valMessages, int ownValue) {
		Map<Integer, Integer> counter = new HashMap<Integer, Integer>();
		counter.put(ownValue, 1);
		for (Integer message : valMessages.values()) {
			if (!counter.containsKey(message)){
				counter.put(message, 1);
			} else {
				counter.put(message, counter.get(message) + 1);
			}
		}
		return maxEntry(counter).getKey();
	}
	
	//Wait until the latch is down, i.e. when an outcome message is sent, or the sender is suspected 
	private boolean collectOutcome(int c) {
		try {
			outcomeLatches.get(c).await();
		} catch (InterruptedException e) {}
		return outcomeMessages.containsKey(c);
	}
}
