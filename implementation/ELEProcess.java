package implementation;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import provided.IFailureDetector;
import provided.Message;

public class ELEProcess extends ProcessImplementation {

	private IFailureDetector detector;

	public ELEProcess(String name, int id, int size) {
		super(name, id, size);
		detector = new EventuallyLeaderElector(this);
	}

	public void begin() {
		detector.begin();
	}

	public synchronized void receive(Message m) {
		String type = m.getType();
		if (type.equals("heartbeat")) {
			detector.receive(m);
		}
	}

	public static void main(String[] args) {
		ELEProcess p1 = new ELEProcess("P1", 1, 3);
		ELEProcess p2 = new ELEProcess("P2", 2, 3);
		ELEProcess p3 = new ELEProcess("P3", 3, 3);

		ExecutorService executor = Executors.newFixedThreadPool(3);

		executor.submit(new ProcessRegisterer(p1));
		executor.submit(new ProcessRegisterer(p2));
		executor.submit(new ProcessRegisterer(p3));
	}

	@Override
	public int consensus() {
		throw new UnsupportedOperationException();
	}
}