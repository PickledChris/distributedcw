package implementation;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

import provided.IFailureDetector;
import provided.Message;
import provided.Process;

public class StrongFailureDectector extends PerfectFailureDectector implements IFailureDetector{

	private Map<Integer, Integer> decisions = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, CountDownLatch> decisionReceivedOrFailed = new ConcurrentHashMap<Integer, CountDownLatch>();

	public StrongFailureDectector(Process p) {
		super(p);
	}
	
	@Override
	public void begin() {
		super.begin();
		for (int i = 1; i <= p.n; i++) {
			decisionReceivedOrFailed.put(i, new CountDownLatch(1));
		}
	}

	@Override
	public void receive(Message m) {
		super.receive(m);
		if (m.getType().equals("VAL")) {
			decisions.put(m.getSource(), new Integer(m.getPayload()));
			decisionReceivedOrFailed.get(m.getSource()).countDown();
		}
		//Utils.out(p.pid, m.toString()); 
	}

	@Override
	public void isSuspected(Integer process) {
		super.isSuspected(process);
		
		decisionReceivedOrFailed.get(process).countDown();
		//Utils.out(String.format("%d is suspected by %d", process, p.pid));
	}

	public int consensus(int value) {
		decisions.put(p.pid, value);
		Map<Integer, Integer> decisionCounter = new HashMap<Integer, Integer>();
		for (int r = 1; r <= p.n; r++) {
			if (r == p.pid) {
				p.broadcast("VAL", String.valueOf(value));
				// This means a faulty process will still return its given value, as it suspects everyone else.
				incrementDecision(decisionCounter, decisions, r); 
			} else {
				// collect
				try {
					decisionReceivedOrFailed.get(r).await();
				} catch (InterruptedException e) {}
				
				incrementDecision(decisionCounter, decisions, r);
			}
		}
		return maxEntry(decisionCounter).getKey();
	}
	
	// If the process r was not suspected, increment the value's decision count
	public static void incrementDecision(Map<Integer, Integer> decisionCounter, 
			Map<Integer, Integer>decisions, int r) {
		Integer decision = decisions.get(r);
		if (decision != null) {
			Integer count = decisionCounter.get(decision);
			if (count != null) {
				decisionCounter.put(decision, count + 1);
			} else {
				decisionCounter.put(decision, 1);
			}
		}
	}

	public static Map.Entry<Integer, Integer> maxEntry(Map<Integer, Integer> decisionCounter) {
		Map.Entry<Integer,Integer> maxEntry = null;
		for (Entry<Integer, Integer> decCount : decisionCounter.entrySet()) {
			 if (maxEntry == null || decCount.getValue().compareTo(maxEntry.getValue()) > 0) {
			        maxEntry = decCount;
			 }
		}
		return maxEntry;
	}
}
