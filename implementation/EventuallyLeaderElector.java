package implementation;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import provided.IFailureDetector;
import provided.Process;
import provided.Utils;

public class EventuallyLeaderElector extends EventuallyPerfectFailureDectector implements IFailureDetector {

	private static final int DELTA = 1000; /* 1sec */
	private int leader;
	private Set<Integer> allProcesses = new HashSet<Integer>();

	public EventuallyLeaderElector(Process p) {
		super(p);
		this.leader = p.n;
		for (int i = 1; i <= p.n; i++) {
				this.allProcesses.add(i);
		}
	}
	
	private class Timeout implements Runnable {
		public void run() {
			long start = System.currentTimeMillis();

			try {
				Thread.sleep(DELTA + Collections.max(processLongestDelay.values())); // Need to improve this?
			} catch (InterruptedException e) { e.printStackTrace();}
			
			for (Entry<Integer, Long> procTime : processLastContact.entrySet()) {
				if (procTime.getValue() < start){
					isSuspected(procTime.getKey());
				} else {
					suspects.remove(procTime.getKey());
				}
				recalculateLeader();
			}
		}
	}
	
	// Need to override to schedule local Timeout()
	@Override
	public void begin() {
		// Set all other processes to have last clocked in now
		resetLastContact();
		initialiseLongestDelay();
		heartbeat.schedule(new PeriodicTask(), 0, DELTA);
		timeout.scheduleAtFixedRate(new Timeout(), 0, DELTA, TimeUnit.MILLISECONDS);
	}	
	
	@Override
	public int getLeader() {
		return leader;
	}
	
	private void recalculateLeader() {
		int oldLeader = leader;
		leader = Collections.max(unsuspected(allProcesses, suspects));
		if (leader != oldLeader){
			Utils.out(String.format("%d, thinks %d is the leader",p.pid, leader));
		}
	}
	
	private Set<Integer> unsuspected(Set<Integer> all, Set<Integer> suspects) {
		Set<Integer> copy = new HashSet<Integer>(all); 
		copy.removeAll(suspects);
		return copy;
	}
	
}
