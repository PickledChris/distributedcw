package implementation;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import provided.IFailureDetector;
import provided.Message;

public class EPFDProcess extends ProcessImplementation {

	private IFailureDetector detector;

	public EPFDProcess(String name, int id, int size) {
		super(name, id, size);
		detector = new EventuallyPerfectFailureDectector(this);
	}

	public void begin() {
		detector.begin();
	}

	public synchronized void receive(Message m) {
		String type = m.getType();
		if (type.equals("heartbeat")) {
			detector.receive(m);
		}
	}

	public static void main(String[] args) {
		
		EPFDProcess p1 = new EPFDProcess("P1", 1, 3);
		EPFDProcess p2 = new EPFDProcess("P2", 2, 3);
		EPFDProcess p3 = new EPFDProcess("P3", 3, 3);
		
		ExecutorService executor = Executors.newFixedThreadPool(3);
		
		executor.submit(new ProcessRegisterer(p1));
		executor.submit(new ProcessRegisterer(p2));
		executor.submit(new ProcessRegisterer(p3));
	}

	@Override
	public int consensus() {
		throw new UnsupportedOperationException();
	}
}