package implementation;

import java.util.concurrent.Callable;

public class ConsensusTask implements Callable<Integer> {

	private ProcessImplementation p;
	public ConsensusTask(ProcessImplementation p) {
		this.p = p;
	}
	
	@Override
	public Integer call() {
		return p.consensus();
	}
}