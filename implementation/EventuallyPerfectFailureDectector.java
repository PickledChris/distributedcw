package implementation;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import provided.IFailureDetector;
import provided.Message;
import provided.Process;
import provided.Utils;

public class EventuallyPerfectFailureDectector implements IFailureDetector {

	protected static final int DELTA = 1000; /* 1sec */
	protected Process p;
	protected Timer heartbeat;
	protected ScheduledExecutorService timeout;
	protected Set<Integer> suspects;
	protected Map<Integer, Long> processLastContact = new ConcurrentHashMap<Integer, Long>();
	protected Map<Integer, Long> processLongestDelay = new ConcurrentHashMap<Integer, Long>();

	public EventuallyPerfectFailureDectector(Process p) {
		this.p = p;
		this.heartbeat = new Timer();
		this.timeout = new ScheduledThreadPoolExecutor(3);
		this.suspects = new HashSet<Integer>();
	}
	
	protected class PeriodicTask extends TimerTask {
		public void run() {
			p.broadcast("heartbeat", String.valueOf(System.currentTimeMillis()));
		}
	}
	
	private class Timeout implements Runnable {
		public void run() {
			long start = System.currentTimeMillis();

			try {
				Thread.sleep(DELTA + Collections.max(processLongestDelay.values())); // Need to improve this?
			} catch (InterruptedException e) { e.printStackTrace();}
			
			for (Entry<Integer, Long> procTime : processLastContact.entrySet()) {
				if (procTime.getValue() < start){
					isSuspected(procTime.getKey());
				} else {
					suspects.remove(procTime.getKey());
				}
			}
		}
	}
	
	@Override
	public void begin() {
		// Set all other processes to have last clocked in now
		resetLastContact();
		initialiseLongestDelay();
		heartbeat.schedule(new PeriodicTask(), 0, DELTA);
		timeout.scheduleAtFixedRate(new Timeout(), 0, DELTA, TimeUnit.MILLISECONDS);
	}
	
	@Override
	public void receive(Message m) {
		
		processLastContact.put(m.getSource(), System.currentTimeMillis());
		
		long delay = (System.currentTimeMillis() - Long.parseLong(m.getPayload()));
		if (processLongestDelay.get(m.getSource()) < delay){
			processLongestDelay.put(m.getSource(), delay);
		}
	}

	@Override
	public boolean isSuspect(Integer process) {
		return suspects.contains(process);
	}

	@Override
	public int getLeader() {
		return -1;
	}

	@Override
	public void isSuspected(Integer process) {
		suspects.add(process);
	}

	@Override
	public int consensus(int value) {
		throw new UnsupportedOperationException();
	}
	
	protected void resetLastContact() {
		for (int i = 1; i <= p.n; i++) {
			if (i != p.pid)
				processLastContact.put(i, System.currentTimeMillis());
		}
	}
	
	protected void initialiseLongestDelay() {
		for (int i = 1; i <= p.n; i++) {
			if (i != p.pid)
				processLongestDelay.put(i, (long) Utils.DELAY);
		}
	}
}
