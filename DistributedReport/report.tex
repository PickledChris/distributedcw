\documentclass[a4wide, 9pt]{report}
\usepackage[margin=0.5in]{geometry}

\usepackage{caption}
\usepackage{listings}
\usepackage{verbatim}
\usepackage{xcolor}
\usepackage{color}

\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here

\newcommand{\superscript}[1]{\ensuremath{^{\textrm{#1}}}}
\newcommand{\subscript}[1]{\ensuremath{_{\textrm{#1}}}}
\newcommand{\var}[1]{\lstinline[basicstyle=\ttfamily]!#1!}


\begin{document}


\begin{center}
{\bf\Large Agreement in asynchronous distributed systems report}\\[0.5cm]
\large Doc 437: Distributed Algorithms\\[0.4cm]
\end{center}
{\it\large\center
\hspace{3.3cm}
Raluca Bolovan (rab109)\hspace{1.5cm}
Christopher Keeley (ck2109)\hspace{1.5cm}
}

\section*{Introduction}
Failure detectors represent a method for solving the consensus problem with crash failures by providing a way to differentiate between processes that are slow and those that are faulty. Each process within the considered system has a failure detector module connected to it that continuously returns a set of suspected processes. This set does not necessary reflect the true state of the world. Furthermore, failure detectors at different processes do not need to concur. \\

\noindent
True failures are modeled using failure patterns of the form F(t) that denotes the set of processes that crashed at time \var{t}. Suspicions are modeled using histories of the type H(q,t) - a set of the processes suspected at time \var{t} by process \var{q}.  \\

\noindent
Failure detectors are characterised in terms of two properties : completeness (suspicion of crashed processes) and accuracy (suspicion of correct processes). 

\subsubsection{Completeness degree}
\begin{itemize}
  \item {\bfseries{Strong:}} Eventually every crashed process will be permanently suspected  by every correct process.
  \item {\bfseries{Weak:}} Eventually every crashed process will be permanently suspected by an existing correct process.
\end{itemize}

\subsubsection{Accuracy degree}
\begin{itemize}
  \item {\bfseries{Strong:}} No process is suspected by a correct process before it crashes.
  \item {\bfseries{Weak:}} Some correct process is never suspected by any non-faulty process. 
 \item {\bfseries{Eventually strong:}} There is a time after which correct processes are never suspected by  any other correct processes. 
  \item {\bfseries{Eventually weak:}} There is a time after which some correct process is never suspected by any other correct process.
\end{itemize}

\noindent
Based on the above classification we can defie the following failure detector classes:
\begin{itemize}
  \item {\bfseries{Perfect (P):}} presents strong completeness and strong accuracy.
  \item {\bfseries{Strong (S):}} presents strong completeness and weak accuracy.
 \item {\bfseries{Eventually perfect ($\diamond$P):}} presents strong completeness and eventually strong accuracy.
  \item {\bfseries{Eventually strong ($\diamond$S):}} presents strong completeness and eventually weak accuracy.
\end{itemize}
 
\section*{Implementation details}

Each instantiation process has an associated main method, which instantiates 3 or 4 of the processes. The logs were created running these and running a separate \var{Registrar}, and in the case of failures, a \var{FaultInjector}.  \\ 

\noindent
\var{PerfectFailureDetector} is implemented using a \var{TimerTask}, which broadcasts a heartbeat with a null payload every \var{DELTA}. It also uses a \var{ScheduledThreadPoolExecutor} to run a \var{Timeout} subclass, which waits for the maximum allowed duration (exactly known as we know the value of \var{Utils.DELAY}), and checks if a heartbeat has been heard from each other process in that time. If not, then that process is added to the set of suspected processes. \\

\begin{figure}
\begin{lstlisting}
[001] Connected.
[002] Connected.
[003] Connected.
[003] Registered.
[001] Registered.
[002] Registered.
> P1<|>OFF
[DBG] Process 1 suspects 2
[DBG] Process 1 suspects 3
[DBG] Process 2 suspects 1
[DBG] Process 3 suspects 1
[DBG] Process 1 suspects 2
[DBG] Process 1 suspects 3
[DBG] Process 2 suspects 1
[DBG] Process 3 suspects 1
> P1<|>ON
\end{lstlisting}
 \caption{PFDProcess log, with FaultInjector lines interspersed}
\end{figure}

\noindent
\var{EventuallyPerfectFailureDetector} is implemented very similarly to \var{PerfectFailureDetector}, except that a heartbeat contains the time that it was sent. When this is received, the delay is calculated and if it is larger than the previous highest, it is recorded. This highest delay is used to calculate a dynamic upper bound which is used in the same way as the static delay in \var{PerfectFailureDetector}. \\

\noindent
\var{EventuallyLeaderElector} is very similar to \var{EventuallyPerfectFailureDetector}, so it was decided to make it a subclass, only changing methods to do with returning and recalculating the leader. Whenever a change is potentially made to the \var{suspects} set, the leader is recalculated. \\

\begin{center}
\begin{figure}
\begin{lstlisting}
[001] Connected.
[002] Connected.
[003] Connected.
[002] Registered.
[003] Registered.
[001] Registered.
[DBG] 1, thinks 3 is the leader
[DBG] 2, thinks 3 is the leader
[DBG] 3, thinks 3 is the leader
P1<|>OFF
[DBG] 1, thinks 1 is the leader
P1<|>ON
[DBG] 1, thinks 2 is the leader
[DBG] 1, thinks 3 is the leader
P3<|>OFF
[DBG] 1, thinks 2 is the leader
[DBG] 2, thinks 2 is the leader
\end{lstlisting}
 \caption{ELEProcess with FaultInjector lines, showing when leader beliefs change}
\end{figure}
\end{center}

\noindent
\var{StrongFailureDetector} uses \var{PerfectFailureDetector} as a failure detector, and is a subclass of it. It also keeps track of the decisions made by the other processes and models whether each other process has responded using a \var{CountDownLatch}, a synchronisation class which blocks on the \var{await()} method until \var{countDown()} has been called enough times, in this case once for each other process. If a process is suspected, the latch is also triggered, so that a failed process does not block indefinitely. The \var{consensus} method is used to determine the agreed upon value as per the rotating coordinators algorithm, with the blocking collect call using the \var{await()} method. \\

\begin{center}
\begin{figure}
\begin{lstlisting}
[001] Connected.
[002] Connected.
[003] Connected.
[001] Registered.
[003] Registered.
[002] Registered.
1: 42
2: 42
3: 42
\end{lstlisting}
 \caption{SFDProcess running normally, all determining the correct values. Were a FaultInjector to disable a process, the output remains the same, as the two remaining processes still work, and the incorrect process will use only its own value}
\end{figure}
\end{center}


\noindent
\var{EventuallyStrongFailureDetector} uses \var{EventuallyPerfectFailureDetector} as a failure detector, and is a subclass of it. It calculates the minimum number of messages that need to be received to use the rotating co-ordinators algorithm, and uses this in a \var{CountDownLatch}, which blocks until there are enough received "VAL" messages for the candidate process to proceed. Like \var{StrongFailureDetector}, it also uses a collection of \var{CountDownLatches} which are used to block on processes which have not sent messages yet. \\

\begin{center}
\begin{figure}
\begin{lstlisting}
[001] Connected.
[002] Connected.
[003] Connected.
[004] Connected.
[002] Registered.
[001] Registered.
[003] Registered.
[004] Registered.
[DBG] Process 3 in iteration 1, m= 2, c=0
[DBG] Process 1 in iteration 1, m= 2, c=0
[DBG] Process 4 in iteration 1, m= 2, c=0
[DBG] Process 1 in iteration 2, m= 3, c=2
[DBG] Process 2 in iteration 2, m= 3, c=0
[DBG] Process 4 in iteration 2, m= 3, c=2
[DBG] Process 1 in iteration 3, m= 4, c=2
[DBG] Process 2 in iteration 3, m= 4, c=3
[DBG] Process 3 in iteration 3, m= 4, c=2
[DBG] Process 2 in iteration 4, m= 1, c=3
[DBG] Process 3 in iteration 4, m= 1, c=2
[DBG] Process 4 in iteration 4, m= 1, c=2
[DBG] Process 1 in iteration 5, m= 2, c=2
[DBG] Process 3 in iteration 5, m= 2, c=2
[DBG] Process 4 in iteration 5, m= 2, c=2
[DBG] Process 2 in iteration 6, m= 3, c=3
1: 42
2: 42
3: 42
4: 42
\end{lstlisting}
 \caption{ESFDProcess running normally, all determining the correct values. Added debug message to make the iterations clearer.}
\end{figure}
\end{center}

\begin{center}
\begin{figure}
\begin{lstlisting}
[001] Connected.
[002] Connected.
[003] Connected.
[004] Connected.
[003] Registered.
[001] Registered.
[002] Registered.
[004] Registered.
>P1<|>OFF
[DBG] Process 3 in iteration 1, m= 2, c=0
[DBG] Process 4 in iteration 1, m= 2, c=0
[DBG] Process 2 in iteration 2, m= 3, c=0
[DBG] Process 4 in iteration 2, m= 3, c=2
[DBG] Process 2 in iteration 3, m= 4, c=3
[DBG] Process 3 in iteration 3, m= 4, c=2
[DBG] Process 2 in iteration 6, m= 3, c=3
[DBG] Process 4 in iteration 5, m= 2, c=2
[DBG] Process 3 in iteration 5, m= 2, c=2
1: failed somewhere
2: 42
3: 42
4: 42
\end{lstlisting}
 \caption{ESFDProcess running with process 1 disabled, the remaining three determining the correct values.  Debug messages to make the iterations clearer.}
\end{figure}
\end{center}

\newpage
\section*{2.2 Analysis tasks}
\subsection*{2.2.1 Relationships between classes}
In order for a failure detector D to emulate another failure detector D', it need not emulate all the histories (outputs) of D'. Our requirement is that all the failure detector histories it outputs be histories of D'. \\

\noindent 
Because all the failure detectors exhibit strong completeness, we cannot differentiate amongst their histories based on this characteristic, and shall only consider their accuracy property for comparison purposes. Due to strong completeness, we have a guarantee that every process that crashes will eventually be suspected by every correct process. Thus, we will only focus on non-faulty processes in order to reason about the hierarchy of the classes of failure detectors. For the purpose of the following proofs, we define p as some non-faulty process. \\



\noindent
\textbf{a) Relationship between P and S} \\
%Choose some non-faulty process p and 
Consider an execution in which S periodically suspects \var{p} (S is allowed to do this if there is some other non-faulty process that it never suspects). Due to the strong accuracy property exhibited by P, if \var{p} has not crashed it will never be included in the list of suspected processes and thus not be part of any output history.(i.e.$\forall$t: $\forall$q$\notin$F(t): p$\notin$ H(q,t)). If S suspects \var{p}, then H1 :$\exists$t$_{1}$,$\exists$q$\notin$F(t$_{1}$):p$\in$H(q,t$_{1}$). The histories of S are supersets of the histories of P, thus P emulates S. The reverse is not true, because S produces at least one history H1 that will not be produced by P.\\
{\bfseries{Conclusion:}} P emulates S and S does not emulate P.\\

\noindent
\textbf{b) Relationship between P and $\diamond$P} \\
Consider an execution in which $\diamond$P periodically suspects \var{p}, i.e. $\exists$t$_{1}$,t$_{1}$ ${<}$t, where \var{t} is the time after which no correct process is suspected in the eventually strongly accurate property. Thus $\exists$t$_{1}$,t$_{1}$$<$t  $\exists$q$\notin$F(t$_{1}$): p$\in$ H(q,t$_{1}$), so $\diamond$P will produce at least one history that will not be included in the histories of P, thus $\diamond$ P does not emulate P. P emulates $\diamond$P in all cases: either the time of "eventually" \var{t} coincides with the first time of P and their histories will be the same, or, all the histories of P will be included in those of $\diamond$ P. \\
{\bfseries{Conclusion:}}   P emulates $\diamond{P}$ and $\diamond{P}$ does not emulate P.   \\

\noindent
\textbf{c) Relationship between S and $\diamond$S} \\
Consider a situation where \var{p} is suspected by $\diamond$ S, and \var{p} is the correct process that S will never suspect. If $\diamond$S suspects process \var{p} before \var{t}, the time after which a correct process is not suspected, as defined by the eventually weakly accurate property, then $\exists$t$_{1}$,t$_{1}$$<$t  $\exists$q$\notin$F(t$_{1}$): p$\in$ H(q,t$_{1}$). $\diamond$ S cannot emulate S, because it contains at least one history in which it suspects \var{p}, whereas S will never contain \var{p} in any of its histories. S emulates $\diamond$ S in all cases:  either the time of "eventually" \var{t} coincides with the first time of S and their histories will be the same, or, all the histories of S will be included in those of $\diamond$ S. \\
{\bfseries{Conclusion:}} S emulates $\diamond$ S and $\diamond$S does not emulate S. \\

\noindent
\textbf{d) Relationship between P and $\diamond$S} \\
The relationships between pairs of failure detectors are transitive. We have already proven that P emulates S and that S emulates $\diamond$ S. By transitivity, we can conclude that P emulates $\diamond$ S. Similarly, we have shown that $\diamond$ S does not emulate S and S does not emulate P. By transitivity, $\diamond$ S does not emulate P. \\
{\bfseries{Conclusion:}} P emulates $\diamond$ S and $\diamond$ S does not emulate P. \\

\noindent
\textbf{e) Relationship between $\diamond P$ and $\diamond$S} \\
In this case we are interested in the executions after time \var{t} for both detectors. Before time \var{t}, process \var{p} is equally likely to be suspected by either failure detector, and thus become part of their histories, so we have no way of differentiating between the two classes of failure detectors. Consider the situation when $\diamond$S suspects \var{p} after time \var{t} (this is allowed as long as there is another correct process that $\diamond$S does not suspect). Now both processes act as their permanently accurate counterparts: $\diamond$P as P and $\diamond$ S as S. The proof is analogous to the one of the relationships between P and S, where we have shown that P emulates S and S  does not emulate P. \\
{\bfseries{Conclusion:}} $\diamond$P emulates $\diamond$S and $\diamond$S  does not emulate $\diamond$P. \\ 


\noindent
\textbf{f) Relationship between S and $\diamond$P} \\
$\diamond$P doesn't emulate S  : Let \var{p} be the correct process that S will never suspect. 
Before time \var{t} in the case of $\diamond$P, \var{p} can be suspected, thus creating a history that will not be produced by S.  S doesn't emulate $\diamond $ P: consider what happens after time \var{t}: $\diamond$P will behave like P, and the proof is analogous to the one in {\bfseries{a}}, where we have shown that S does not emulate P, and so it will not emulate $\diamond$P either. \\
{\bfseries{Conclusion:}} There is no relationship between S and $\diamond$ P : S does not emulate $\diamond$ P and $\diamond$P does not emulate S.









\newpage
\subsection*{2.2.2 Weak completeness}
\definecolor{gr}{rgb}{0.84,0.84,0.84}


\lstset{language=Java,tabsize=1, 
caption= Weak completeness augmented to strong completeness ,
frame=single,
breaklines=true,
showtabs=false,
showspaces=false,
showstringspaces=false,
basicstyle=\scriptsize}

\begin{lstlisting}[mathescape=true,basicstyle=\scriptsize, numberstyle=\tiny,backgroundcolor=\color{black!20},numbers=left,frame=lrtb]
each process i executes:
output$_i$:={}   % output$_i$ emulates D$^{'}$$_{i}$, initially empty 

cobegin
while (true) do   % step 1
      % i queries its local failure detector D$_{i}$
      suspects$_{i}$ := D$_{i}$
      send(i, suspects$_{i}$) to all processes in P

%step 2
upon receive(p, suspects$_{p}$) for some process p   
        output$_{i}$ := (output$_{i}$ $\bigcup$  suspects$_{p}$) \ {p}

coend
\end{lstlisting} 

\noindent
The algorithm is comprised of two steps(tasks). Each process i periodically sends messages of the type (\var{i}, \var{suspects}$_{i}$) to every other process in the system; where \var{suspects}$_{i}$ is the list of processes that \var{i} suspects, according to its local failure detector D$_{i}$. Whenever \var{i} receives a message from a process \var{p} of the form (\var{p}, \var{suspects}$_{p}$), it extends its list of output$_{i}$ with \var{suspects}$_{p}$ and also removes \var{p} from it; this is equivalent to a heartbeat message, because a process that was wrongfully suspected will be removed from the output.\\

\noindent
From the weak completeness property satisfied by D,  we know that every process that crashes will be eventually and  permanently suspected by some correct process, that we shall call \var{q}. We shall call the process that \var{q} suspects \var{p} and the time after which \var{q} permanently suspects \var{p} as \var{t}. Process \var{p} crashing signifies that we have a time \var{t'} after which no process in P will receive a message from \var{p}. We shall now consider what happens when executing step 1 by \var{q} at the time t$_{suspect_p}$ that is the maximum between \var{t} and \var{t'}. Process \var{q} will send a message of the form (\var{q}, \var{suspects}$_{q}$) with \var{p} included in the list of suspects to each of the processes in P. Each process will eventually receive \var{q}'s message and thus add \var{p} to their output list. Furthermore, because \var{t'} $\leq$  t$_{suspect_p}$, this means that there will never be the case that a process will remove \var{p} from their output list, which  means that there will be a time after which every process in P will permanently suspect \var{p}, achieving {\bfseries{strong completeness}}.\\

\noindent
{\bfseries{Constant accuracy (strong $\&$ weak) is maintained}}: Let \var{p} be a process in P. Consider a time \var{t} before which no process suspects \var{p} in its history implying that $\forall$q, p $\notin$ suspects$_{q}$. This means that no process \var{q} adds \var{p} to output$_{q}$ before time \var{t}. This shows that the algorithm does not influence the strong or weak accuracy properties of a failure detector.\\

\noindent
{\bfseries{Eventual accuracy(strong $\&$ weak) is maintained}}: Let \var{p} be a correct process in P. Consider a time \var{t} after which no correct process suspects P in its histories.  This means that there will be a time \var{t'}  after which these processes will not receive a message containing \var{p} in the lists of suspected processes. Suppose \var{q} is a correct process. Consider the execution of step 1 by \var{p} after time t' : \var{p} sends a message of the form (\var{p}, \var{suspects}$_{p}$) to \var{q}. The last line of step 2 shows that \var{p} will be removed from \var{output}$_{q}$. Because \var{q} will never receive a message containing \var{p} in the suspect list, its \var{output}$_{q}$ shall not contain \var{p} after \var{t'}.  This shows that the eventual accuracy properties are not influenced by the algorithm, and thus maintained.


\end{document}  